[![pipeline status](https://gitlab.com/tue-umphy/co2mofetten/software/co2project/badges/main/pipeline.svg)](https://gitlab.com/tue-umphy/co2mofetten/software/co2project/-/pipelines)
[![coverage report](https://gitlab.com/tue-umphy/co2mofetten/software/co2project/badges/main/coverage.svg)](https://tue-umphy.gitlab.io/co2mofetten/software/co2project/coverage-report/)
[![documentation](https://img.shields.io/badge/documentation-here%20on%20GitLab-brightgreen.svg)](https://tue-umphy.gitlab.io/co2mofetten/software/co2project)

# 📈 `co2project` - Utilities for Data Analysis in the CO₂ Project 🛠

## What can `co2project` do?

... TBD ...

## :package: Installation

To install this package directly from GitLab, run

```bash
# make sure you have pip installed
# Debian/Ubuntu:  sudo apt update && sudo apt install python3-pip
# Manjaro/ARch:  sudo pacman -Syu python-pip
python3 -m pip install --user -U git+https://gitlab.com/tue-umphy/co2mofetten/software/co2project
```

You may also use [our workgroup Arch/Manjaro repository](https://gitlab.com/tue-umphy/workgroup-software/repository) and install the `python-co2project` package with your favourite software installer.

## :book: Documentation

Documentation can be found [here on GitLab](https://tue-umphy.gitlab.io/co2mofetten/software/co2project).

## :heavy_plus_sign: Contributing

If you'd like to contribute, e.g. by adding new features or fixing bugs, read the [`CONTRIBUTING.md`-file](https://gitlab.com/tue-umphy/co2mofetten/software/co2project/-/blob/main/CONTRIBUTING.md).
