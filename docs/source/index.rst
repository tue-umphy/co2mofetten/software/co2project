Welcome to co2project's documentation!
======================================

:mod:`co2project` is a Python package for data analysis in the `CO₂ project <https://gitlab.com/tue-umphy/co2mofetten>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   examples
   api/modules
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
