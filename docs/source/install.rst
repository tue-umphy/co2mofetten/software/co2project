Installation
============


From our Workgroup Repository
+++++++++++++++++++++++++++++

If you are using our `Manjaro Workgroup Repository
<https://gitlab.com/tue-umphy/workgroup-software/repository>`_, chances are you
already have :mod:`co2project` installed. It is available in the repository as
``python-co2project`` package. Install it with your favourite software installer.


Directly From GitLab
++++++++++++++++++++

You can install the latest development version of :mod:`co2project` via :mod:`pip`

.. code-block:: sh

    python3 -m pip install --user -U git+https://gitlab.com/tue-umphy/software/co2project

This will install :mod:`co2project` directly from GitLab.

.. hint::

    Depending on your setup it might be necessary to install the :mod:`pip` module
    first:

    .. code-block:: sh

        # Debian/Ubuntu
        sudo apt-get install python3-pip
        # Manjaro/Arch
        sudo pacman -Syu python-pip

    Or see `Installing PIP`_.

    It is also a good idea to update :mod:`pip`:

    .. code-block:: sh

        python3 -m pip install --user -U pip

    .. _Installing PIP: https://pip.pypa.io/en/stable/installing/

.. hint::

    Instead of the lengthy ``python3 -m pip install --user -U`` you might need
    to use a plain ``pip install``. Try that if the former fails.


If that doesn't work
--------------------

Try cloning the repository and install it from there:

.. code-block:: sh

    # clone the repository
    git clone https://gitlab.com/tue-umphy/software/co2project
    # go into the repository
    cd co2project
    # install from the repository
    python3 -m pip install --user .


If that doesn't work either
---------------------------

If installing from the repository didn't work, you can try downloading the latest built package from `here
<https://gitlab.com/tue-umphy/software/co2project/-/jobs/artifacts/main/browse/dist?job=dist>`_:
Download the ``.tar.gz`` file and then execute

.. code-block:: sh

    python3 -m pip install --user /path/to/the/file/co2project.tar.gz

If even that doesn't work and the error message is not obvious, you probably
either got something wrong or your system configured in a weird way. Might be
some weird virtual environment or
Anaconda setup or a super old Python version, etc...
