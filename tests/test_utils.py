# system modules
import unittest

# internal modules
from co2project import utils

# external modules


class UtilsTest(unittest.TestCase):
    def test_tree_joints(self):
        self.assertEqual(utils.tree_joints(0, 2), 0)
        self.assertEqual(utils.tree_joints(0, 10), 0)
        self.assertEqual(utils.tree_joints(10, 2), 11)
        self.assertEqual(utils.tree_joints(10, 3), 7)
        self.assertEqual(utils.tree_joints(25, 2), 27)
        self.assertEqual(utils.tree_joints(25, 5), 6)
