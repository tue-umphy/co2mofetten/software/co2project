# system modules
import os
import re
from fnmatch import fnmatch
import time
import pickle
import textwrap
import inspect
import logging
from pathlib import Path
import hashlib
import json
import itertools
import functools
from typing import Optional, Union
from dataclasses import dataclass, field

# internal modules

# external modules
import bz2
import gzip
import lzma
import zstandard

logger = logging.getLogger(__name__)

DEFAULT_CACHE_DIR = (
    Path(os.environ.get("XDG_CACHE_HOME", Path("~/.cache").expanduser()))
    / "co2project"
    / "cache"
)


def pretty_bytes(n: int) -> str:
    prefixes = {
        p.strip(): functools.reduce((lambda x, y: x * 1024), range(m), 1)
        for m, p in enumerate(" kMGTPE", start=0)
    }
    value, prefix = float(n), ""
    for (pprefix, pmult), (prefix, mult) in zip(
        list(prefixes.items())[:-1], list(prefixes.items())[1:]
    ):
        if (v := n / mult) < 1:
            value, prefix = n / pmult, pprefix
            break
    return f"{value:.1f}".rstrip(".0") + f"{prefix.strip()}B"


def objectsummary(x: object) -> str:
    parts = [type(x).__name__]
    try:
        parts.append(f"{len(x)} elements")  # type: ignore[arg-type]
    except Exception as e:
        pass
    return ", ".join(map(str, parts))


def cached(
    directory: Union[str, Path] = DEFAULT_CACHE_DIR,
    min_runtime_seconds=0,
    ignore=None,
):
    """
    Cache the decorated function's results in a pickle file tree. Lookup is
    performed based on the functions module and name, its source code and
    arguments, but not its dependencies.

    The decorated function gets new arguments ``cache`` to control caching
    (``False`` to disable cache lookup, ``True`` for forced cache lookup,
    ``None`` (the default) for lookup and storage only if the function ran for
    longer than ``[cache_]min_runtime_seconds``, ``"remake"`` to force-remake the
    cache), ``cachedir`` (path to the cache directory) and ``[cache_]ignore``
    to provide a sequence of argument names (glob patterns) ignore when
    serializing to a key.
    """
    directory = Path(directory)

    def decorator(decorated_fun):
        funcname = f"{decorated_fun.__module__}.{decorated_fun.__name__}"
        signature = inspect.signature(decorated_fun)

        @functools.wraps(decorated_fun)
        def wrapper(*args, **kwargs):
            cache = kwargs.pop("cache", None)

            cachedir = directory
            if cachedir_ := kwargs.pop("cachedir", None):
                try:
                    cachedir = Path(cachedir_)
                except Exception as e:
                    logger.error(
                        f"Weird cachedir={cachedir_!r} given. Using {directory} instead."
                    )
            for p in funcname.split("."):
                cachedir /= p

            cache_min_runtime_seconds = min_runtime_seconds
            if cache_min_runtime_seconds_ := kwargs.pop(
                "cache_min_runtime_seconds", None
            ):
                try:
                    cache_min_runtime_seconds = float(
                        cache_min_runtime_seconds_
                    )
                except Exception as e:
                    logger.error(
                        f"Weird cache_min_runtime_seconds={cache_min_runtime_seconds_!r} given. "
                        f"Using {min_runtime_seconds} instead."
                    )

            cache_ignore = set(kwargs.pop("cache_ignore", ignore) or set())

            if cache in {False, "ignore"}:
                logger.debug(f"Skip cache for {funcname} because {cache=!r}")
                return decorated_fun(*args, **kwargs)

            try:
                arguments = signature.bind(*args, **kwargs)
                arguments.apply_defaults()
            except TypeError as e:
                logger.warning(
                    f"Can't bind signature of {funcname}: {e!r}\n\n"
                )
                arguments = None

            if not arguments:
                logger.warning(
                    f"Without argument information, caching can't be performed. Skip cache for {funcname}"
                )
                return decorated_fun(*args, **kwargs)

            funcsrc = inspect.getsource(decorated_fun)
            call_serialized = [
                funcname,
                funcsrc,
                arguments.args,
                {
                    k: v
                    for k, v in arguments.kwargs.items()
                    if not any(fnmatch(k, p) for p in cache_ignore)
                },
            ]
            argstr = ",".join(
                textwrap.shorten(s, 100)
                for s in itertools.chain(
                    map(repr, arguments.args),
                    (
                        re.sub(r"\r\n", r"↩", f"{k}={v!r}")
                        for k, v in arguments.kwargs.items()
                    ),
                )
            )[:300]

            @dataclass
            class defaultformatter:
                unreprable: set = field(default_factory=set)

                def __call__(self, x):
                    x_ = repr(x)
                    if re.search(r"<.*\s+at\s+0x[a-z0-9]+.*>", x_):
                        self.unreprable.add(x_)
                    return x_

            formatter = defaultformatter()

            call_json = json.dumps(call_serialized, default=formatter)

            if formatter.unreprable:
                logger.warning(
                    f"Caching {funcname}({argstr}) does not make sense due to {len(formatter.unreprable)} "
                    f"meaningless repr()s in its arguments:\n{(chr(10)).join(('·'+str(s)) for s in formatter.unreprable)}"
                )
                return decorated_fun(*args, **kwargs)

            h = hashlib.sha256()
            h.update(call_json.encode(encoding="utf-8", errors="ignore"))
            call_id = h.hexdigest()
            cachefilename = f"{call_id}.pickle"

            def remove(p, reason=""):
                errors = set()
                try:
                    p.unlink()
                    logger.debug(f"Removed {p} ({reason})")
                    return errors
                except Exception as e:
                    errors.add(e)
                try:
                    shutil.rmtree(str(p))
                    logger.debug(f"Removed {p} ({reason})")
                    return errors
                except Exception as e:
                    errors.add(e)
                if errors:
                    logger.error(
                        f"Couldn't remove {p} ({reason}): {chr(10).join(map(repr,errors))}"
                    )
                return errors

            if cache in {"remake", "invalidate", "redo", "delete"}:
                for p in cachedir.glob(f"{cachefilename}*"):
                    remove(p, reason=f"{cache=!r}")

            openers = {
                ".gz": gzip.open,
                ".gzip": gzip.open,
                ".bz2": bz2.open,
                ".bzip": bz2.open,
                ".bzip2": bz2.open,
                ".xz": lzma.open,
                ".zst": zstandard.open,
                ".pickle": open,
            }

            def result_from_cachefile():
                for p in cachedir.glob(f"{cachefilename}*"):
                    logger.debug(f"found cachefile {p}")
                    if not ((p.is_file() or p.is_symlink()) and p.exists()):
                        logger.warning(
                            f"{p} is not an existing file or symlink? 🤨 Removing it."
                        )
                        remove(p, reason="not an existing file or symlink")
                        continue
                    if not (opener := openers.get(p.suffix)):
                        logger.warning(
                            f"Don't know how to open {p} 🤷 Trying normal binary reading."
                        )
                        opener = open
                    try:
                        with opener(str(p), "rb") as fh:
                            yield p, pickle.load(fh)
                    except Exception as e:
                        logger.error(
                            f"Couldn't read {funcname}({argstr}) call cache from {p}: {e!r}"
                        )
                        continue

            # cache lookup
            try:
                cachefile, result = next(result_from_cachefile())
                resultsummary = objectsummary(result)
                try:
                    cachefilesize = cachefile.stat().st_size
                except Exception as e:
                    logger.error(
                        f"Couldn't find out file size of {cachefile}: {e!r}"
                    )
                    cachefilesize = None
                logger.info(
                    f"Using cached {funcname}({argstr}) result [{resultsummary}] from {cachefile}"
                    + (
                        f" ({pretty_bytes(cachefilesize)})"
                        if cachefilesize
                        else ""
                    )
                )
                return result
            except StopIteration:
                logger.info(f"no cache available for {funcname}({argstr})")

            # call original function
            logger.info(f"Executing {funcname}({argstr})")
            time_before = time.perf_counter()
            result = decorated_fun(*args, **kwargs)
            resultsummary = objectsummary(result)
            if (
                cache is None
                and (time_taken := time.perf_counter() - time_before)
                < cache_min_runtime_seconds
            ):
                logger.debug(
                    f"{funcname}({argstr}) took only {time_taken:.1f}s, not worth storing in the cache"
                )
                return result

            # delete all previous cachefiles
            errors = set()
            for p in (old_cachefiles := cachedir.glob(f"{cachefilename}*")):
                errors.update(remove(p, reason="purging old cache files"))
            if errors:
                logger.warning(
                    f"Couldn't delete all {len(old_cachefiles)} old cachefiles for {funcname}({argstr}) call, "
                    f"{len(errors)} errors:\n{chr(10).join(map(repr,errors))}"
                )

            # store new cachefile
            if not cachedir.exists():
                try:
                    logger.debug(f"Creating cache dir {cachedir}")
                    cachedir.mkdir(parents=True, exist_ok=True)
                except Exception as e:
                    logger.error(
                        f"Couldn't create dir {cachedir}, continuing anyway"
                    )
            saved = False
            # opinionated order: zstd is fastest, gz good compromise, others are slow
            for suffix in ".zst .gz .xz .bzip .pickle".split():
                if not (opener := openers.get(suffix)):
                    logger.warning(
                        f"Don't know how to save {suffix!r} files 🤷"
                    )
                    continue
                cachefile = cachedir / Path(cachefilename).with_suffix(
                    (".pickle" + suffix) if suffix != ".pickle" else suffix
                )
                try:
                    with opener(str(cachefile), "wb") as fh:
                        pickle.dump(result, fh)
                    saved = True
                    logger.info(
                        f"Successfully stored result of {funcname}({argstr}) [{resultsummary}] to {cachefile}"
                    )
                    break
                except Exception as e:
                    logger.error(
                        f"Couldn't store result of {funcname}({argstr}) [{resultsummary}] to {cachefile}: {e!r}"
                    )
            if not saved:
                logger.error(
                    f"Couldn't store result of {funcname}({argstr}) [{resultsummary}] to cache"
                )

            return result

        return wrapper

    return decorator
