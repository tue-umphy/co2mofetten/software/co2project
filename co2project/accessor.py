# system modules
import functools
import inspect
import textwrap
import warnings
import logging

# internal modules

# external modules
import parmesan
from pandas.api.extensions import (
    register_dataframe_accessor,
    register_series_accessor,
)
import numpy as np
import pandas as pd

logger = logging.getLogger(__name__)

CO2PROJECT_ACCESSOR_NAME = "co2project"
"""
The name under which the :class:`Co2ProjectAccessor` is registered with
:func:`pandas.api.extensions.register_dataframe_accessor`.
"""


# Implementation taken from PARMESAN
@register_dataframe_accessor(CO2PROJECT_ACCESSOR_NAME)
@register_series_accessor(CO2PROJECT_ACCESSOR_NAME)
class Co2ProjectAccessor:
    """
    This is a :mod:`pandas.DataFrame`  and :mod:`pandas.Series` accessor,
    meaning :mod:`pandas.DataFrame`  and :mod:`pandas.Series` objects will get
    an attribute named like the content of :any:`CO2PROJECT_ACCESSOR_NAME`.
    """

    def __init__(self, obj):
        self.obj = obj

    @classmethod
    def register(cls, decorated_function):
        """
        Add a function as method to this class, which when called is passed the
        pandas object as first argument.

        A usage hint is appended to the decorated function's docstring.

        .. hint::

            This can be used as a decorator:

            .. code-block:: python

                @co2projectAccessor.register
                def my_function(dataframe, arg1, keywordarg=None):
                    ...
                    return dataframe.mean()

            Then, ``my_function`` can be applied directly from a
            :class:`pandas.DataFrame` or a :class:`pandas.Series`:

            .. code-block:: python

                df.co2project.my_function(arg1=..., keywordarg=...)
        """
        if not hasattr(decorated_function, "__call__"):
            raise ValueError(
                "Decorated object {func.__name__} is not a function".format(
                    func=decorated_function
                )
            )
        if hasattr(cls, decorated_function.__name__):
            if hasattr(
                getattr(cls, decorated_function.__name__),
                "__co2project_accessor_registered",
            ):
                warnings.warn(
                    "Overwriting {cls.__name__}.{func.__name__}".format(
                        cls=cls, func=decorated_function
                    ),
                )
            else:
                raise ValueError(
                    "{cls.__name__} already has an "
                    "attribute called {func.__name__}, "
                    "refuse to overwrite".format(
                        cls=cls, func=decorated_function
                    )
                )

        argspec = inspect.getfullargspec(decorated_function)

        if len(argspec.args) < 1:
            raise ValueError(
                "Decorated function {func.__name__} only takes {n} arguments, "
                "To be registered it needs to take "
                "the pandas object as first argument.".format(
                    func=decorated_function, n=len(argspec.args)
                )
            )

        decorated_function.__doc__ = parmesan.utils.string.add_to_docstring(
            docstring=getattr(decorated_function, "__doc__", "") or "",
            extra_doc=textwrap.dedent(
                """
                .. hint::

                    This function from
                    :any:`{func.__module__}.{func.__name__}` is
                    :any:`Co2ProjectAccessor.register` ed, meaning you can
                    use it directly from a :class:`pandas.DataFrame` or
                    :class:`pandas.Series` like

                    .. code-block:: python

                        dataframe.{accessor_name}.{func.__name__}({args})
                        dataframe["column"].{accessor_name}.{func.__name__}({args})

                    However it is still usable as usual like

                    .. code-block:: python

                        from {func.__module__} import {func.__name__}
                        {func.__name__}(dataframe, {args})
                        {func.__name__}(dataframe["column"], {args})
                """.format(
                    accessor_name=CO2PROJECT_ACCESSOR_NAME,
                    func=decorated_function,
                    args=", ".join(
                        argspec.args[1:]
                        + (
                            ["..."]
                            if (argspec.varargs or argspec.varkw)
                            else []
                        )
                    ),
                ),
            ),
        )

        @functools.wraps(decorated_function)
        def wrapper(self, *args, **kwargs):
            return decorated_function(self.obj, *args, **kwargs)

        logger.debug(
            "Registering function {}".format(decorated_function.__name__)
        )
        setattr(cls, decorated_function.__name__, wrapper)
        setattr(decorated_function, "__co2project_accessor_registered", True)
        setattr(wrapper, "__co2project_accessor_registered", True)
        return decorated_function
