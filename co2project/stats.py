# system modules
import logging

# internal modules

# external modules
import sympy

logger = logging.getLogger(__name__)


def maximum_error_equation(
    eq,
    variables=None,
    relative=False,
    replace_relative_error=True,
    error_format="Δ{}_max",
    rel_error_format="Δ{}_max_rel",
    return_function=False,
):
    """
    Given an equation, turn it into an equation for maximum absolute or
    relative error estimation.

    Args:
        variables (sequence of symbols, optional): variables to consider
        relative (bool, optional): whether to make a relative error equation
            instead of absolute error
        replace_relative_error (bool, optional): whether to replace relative
            error terms with a relative error variable
        error_format, rel_error_format (str or callable, optional): formatters
            for error symbols. Either :any:`str.format` string or callable that
            will be given the variable symbol as argument.
        return_function (bool, optional): whether to also return a
            :any:`sympy.lambdify`ed function to calculate the error.

    Returns:
        sympy.Eq  : maximum error equation
        sympy.Eq, callable  : if ``return_function`` is True

    """
    error_formatter = (
        error_format.format if isinstance(error_format, str) else error_format
    )
    rel_error_formatter = (
        rel_error_format.format
        if isinstance(rel_error_format, str)
        else rel_error_format
    )
    if (n := len(eq.lhs.free_symbols)) != 1:
        raise ValueError(
            f"The left-hand side ({eq.lhs!r}) doesn't have exactly 1 free symbols but {n} (eq.lhs.free_symbols)."
        )
    errorterms = []
    variables = variables or eq.rhs.free_symbols
    error_variables = []
    for x in variables:
        Δx = sympy.symbols(error_formatter(x), positive=True)
        error_variables.append(Δx)
        term = sympy.Abs(sympy.diff(eq.rhs, x)) * Δx
        if relative:
            Δx_rel = sympy.symbols(rel_error_formatter(x), positive=True)
            error_variables.append(Δx_rel)
            Δx_rel_eq = sympy.Eq(Δx_rel, Δx / x)
            term /= eq.rhs  # divide by original formula
            coeff = sympy.UnevaluatedExpr(term.coeff(Δx_rel_eq.rhs))
            if (res := (coeff * Δx_rel_eq.rhs).doit()) == term:
                Δx_rel_sym = (
                    Δx_rel_eq.lhs if replace_relative_error else Δx_rel_eq.rhs
                )
                term = Δx_rel_sym if coeff.doit() == 1 else coeff * Δx_rel_sym
        else:
            coeff = sympy.UnevaluatedExpr(term.coeff(Δx))
            if (coeff * Δx).doit() == term:
                term = Δx if coeff.doit() == 1 else coeff * Δx
        errorterms.append(term)

    Δy = sympy.symbols(error_formatter(eq.lhs), positive=True)
    if relative:
        if replace_relative_error:
            Δy = sympy.symbols(rel_error_formatter(eq.lhs), positive=True)
        else:
            Δy /= sympy.UnevaluatedExpr(eq.rhs)
    ret = (error_eq := sympy.Eq(Δy, sum(errorterms)))
    if return_function:
        error_eq = error_eq.doit()
        variables = set(list(variables) + error_variables).intersection(
            error_eq.rhs.free_symbols
        )
        func = sympy.lambdify(sorted(variables, key=str), error_eq.rhs)
        ret = (ret, func)
    return ret
