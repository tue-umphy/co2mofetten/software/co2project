# system modules
try:
    from co2project.version import __version__
except ModuleNotFoundError:
    __version__ = "0.0.0"

# internal modules
import co2project.logging
import co2project.input
import co2project.output
import co2project.accessor
import co2project.processing
import co2project.utils
import co2project.stats
import co2project.cache

# external modules
