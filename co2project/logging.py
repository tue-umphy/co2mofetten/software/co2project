# system modules
import logging

# internal modules

# external modules
from rich.logging import RichHandler


def setup(console=None, **kwargs):
    # reset handlers
    for handler in logging.root.handlers:
        logging.root.removeHandler(handler)
    logging.basicConfig(
        **{
            **dict(
                level="NOTSET",
                format="%(message)s",
                datefmt="[%X]",
                handlers=[RichHandler(console=console)],
            ),
            **kwargs,
        }
    )
