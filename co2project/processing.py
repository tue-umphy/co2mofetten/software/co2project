# system modules
import logging

# internal modules
from co2project.accessor import Co2ProjectAccessor

# external modules
import pandas as pd

logger = logging.getLogger(__name__)


@Co2ProjectAccessor.register
def remove_unused_categories(df, inplace=False):
    """
    Remove all unused categories from all columns

    Args:
        df (pandas.DataFrame or pandas.Series): the data to clean
        inplace (bool, optional): whether to apply the changes in-place. Only
            works with dataframe input.


    Returns:
        pandas.DataFrame: the cleaned dataframe
        pandas.Series: if it was a series
    """
    if hasattr(df, "cat"):
        if inplace:
            logger.warning(f"Can't use inplace=True with series. Ignoring.")
        return df.cat.remove_unused_categories()
    if not inplace:
        df = df.copy()
    for col in df:
        if hasattr(df[col], "cat"):
            categories_before = df[col].cat.categories
            df[col] = df[col].cat.remove_unused_categories()
            if diff := categories_before.size - df[col].cat.categories.size:
                logger.debug(f"Removed {diff} unused categories from {col!r}")
    return df


@Co2ProjectAccessor.register
def insert(df, positions, value, inplace=False):
    """
    Insert rows with a given ``value`` at certain index positions.

    Args:
        df (pandas.DataFrame of pandas.Series): the data
        positions: something that can be used by :any:`pandas.DataFrame.loc`,
            so e.g. a date string ``"2022-01-01T00:00:00"`` or a :any:`slice`
            of that etc.
        inplace (bool, optional): apply modifications in-place.

    .. note::

        Also tries to work around the weird ``fill_value must be a scalar``
        error happening with pint-pandas units attached.
    """
    if not inplace:
        df = df.copy()
    try:
        if hasattr(df, "to_frame"):
            df.loc[positions] = value
        else:
            df.loc[positions, :] = value
    except ValueError as e:
        # see: https://github.com/hgrecco/pint-pandas/issues/126
        if "fill_value must be a scalar" in str(e):
            if inplace:  # shady magic to get inplace=True working
                df.__dict__.update(df.astype(float).__dict__)
                df.loc[positions, :] = value
                df.__dict__.update(df.astype(olddtype).__dict__)
            else:  # go via temporary dataframe as it works there
                df = df.to_frame()
                df.loc[positions, :] = value
                df = df.squeeze()
        else:
            raise
    return df


@Co2ProjectAccessor.register
def breakout_column(df, column, squeeze=True):
    """
    Turn the unique values of a given data column into separate columns.

    Args:
        df (pandas.DataFrame):  the dataframe to operate on
        column (str): the column to whose values to turn into individual
            columns
        squeeze (bool, optional): when ``True`` (the default), put redundant
            column information into the new column name and drop colums without
            information. If ``False``, even columns only one unique value are
            put into a MultiIndex.

    Returns:
        pandas.DataFrame : a dataframe with new columns according to the unique
        values in the given ``column``. If there are more than one extra column
        in the given dataframe, the resulting dataframe has a
        :any:`pandas.MultiIndex` for the columns. Consider
        :any:`pandas.DataFrame.drop` ing unwanted columns before calling
        :any:`breakout_column`.

    .. hint::

        Use this function after loading data with :any:`read_mosquitto_logs`,
        see the example there.
    """
    if hasattr(df, "to_frame"):
        df = df.to_frame()

    def parts():
        for n, g in df.groupby(column, observed=True):
            g = g.drop(columns=[column])
            if len(g.columns) < 1:
                continue
            if len(g.columns) > 1 and squeeze:
                g.dropna(axis="columns", how="all", inplace=True)
                for col in g:
                    if (u := g[col].dropna().unique()).size == 1:
                        n = f"{n} {col}={u[0]}"
                        g.drop(columns=col, inplace=True)
            if len(g.columns) == 1:
                g = g.squeeze(axis="columns").rename(n).to_frame()
            else:
                g.columns = pd.MultiIndex.from_product([[n], g.columns])
            yield g

    return pd.concat(parts(), axis="columns")
