# system modules
import bz2
import lzma
import gzip
import mimetypes
import pathlib
import re
import functools
import glob
import pathlib
import logging
import collections
import math
import warnings
from fnmatch import fnmatch

# internal modules
from co2project import utils
from co2project.cache import cached

# external modules
from rich.console import Console
from rich.progress import Progress
from rich.text import Text
from rich.progress import (
    BarColumn,
    Progress,
    ProgressColumn,
    TaskProgressColumn,
    TextColumn,
    TimeRemainingColumn,
)
import pandas as pd
import numpy as np
import zstandard

logger = logging.getLogger(__name__)

try:
    import magic
except ImportError as e:
    logger.warning(
        f"python-magic is not installed: {e!r}, MimeType deduction might be imprecise"
    )


def mimetype(x):
    """
    Try to determine MimeType of a given file.

    Args:
        x (pathlib.Path or str or file-handle): the file to examine

    Returns:
        str : the MimeType if successful
        None: if unsuccessful
    """
    mimetype = None
    try:
        import magic

        if hasattr(x, "read") and hasattr(x, "tell"):
            oldpos = x.tell()
            x.seek(0)
            return magic.from_buffer(x.read(2048), mime=True)
            x.seek(oldpos)
        else:
            path = pathlib.Path(x)
            if path.exists():
                return magic.from_file(path.resolve(), mime=True)
            else:
                raise ValueError(
                    f"{path} doesn't exist, can't use magic library"
                )
    except (ImportError, ValueError):
        path = pathlib.Path(x)
        if not (mimetype := mimetypes.guess_type(path)[0]):
            if ext := path.suffix.lstrip("."):
                for extensions, mt in {
                    ("gz", "gzip"): "application/gzip",
                    ("zst", "zstd"): "application/zstd",
                    ("bz2", "bz"): "application/x-bzip2",
                    ("xz",): "application/x-xz",
                }.items():
                    if ext in extensions:
                        return mt
    return mimetype


def opener(x, mimetype=None):
    """
    Return a function that can open the given file for reading.
    Uses :any:`mimetype` to figure out the file type and then :any:`gzip.open`,
    :any:`bzip2.open`, :any:`zstandard.open`

    Args:
        x (str or file-handle): the file to open
        mimetype (str, optional): if you already know the mimetype

    Returns:
        callable  : function that the file can be passed to open
    """
    if mt := (mimetype or mimetype(x)):
        for pattern, opener in {
            "application/zstd": zstandard.open,
            "application/x-bzip2": bz2.open,
            "application/gzip": gzip.open,
            "application/x-xz": lzma.open,
            "text/.*": open,
        }.items():
            if re.fullmatch(pattern, mt, flags=re.IGNORECASE):
                return opener
    logger.warning(f"🤷 Don't know how to open file {x!r} of type {mt!r}. ")
    return open


def concat_dataframes(dfs):
    """
    Concatenate dataframes, ignoring empty ones

    Args:
        dfs (sequence of pd.DataFrame): the dataframes to concatenate

    Returns:
        pandas.DataFrame : the concatenated dataframe
    """
    return pd.concat([df for df in dfs if len(df.index) > 0])


class MofNCompleteColumnGrowing(ProgressColumn):
    """
    Re-implementatino of rich.progress.MofNCompleteColumn but with an
    ever-growing column width to prevent the layout from jumping around
    """

    def render(self, task):
        completed = int(task.completed or 0)
        total = "?" if task.total is None else int(task.total)
        width = max(len(str(total)), len(str(completed)))
        try:
            self._w = max(self._w, width)
        except AttributeError:
            self._w = width
        return Text(
            f"{str(completed):>{self._w}s}/{str(total):{self._w}s}",
            style="progress.download",
        )


READ_CSV_FILES_UNIT_EXTRACTORS = {re.compile(r"\[([^\]]+)\]"): True}
"""
Default unit extractor for :any:`read_csv_files`.
"""


def sanitize_units(unit):
    """
    Try to turn a unit string into something :mod:`pint` can understand.
    """
    unit = unit.replace("*C", "°C")
    unit = unit.replace("Ohm", "ohm")
    unit = unit.replace("%", "percent")
    unit = unit.replace("dbi", "dBi")
    return unit


@cached(ignore={"progress", "console"})
def read_csv_files(
    *patterns,
    progress=None,
    console=None,
    concat_in_chunks=True,
    categorical=None,
    reader=pd.read_csv,
    reader_pass_path_as=None,
    merger=pd.concat,
    units=READ_CSV_FILES_UNIT_EXTRACTORS,
    unit_sanitizer=sanitize_units,
    to_numeric=None,
    fix_parse_dates=True,
    sort_index=True,
    **reader_kwargs,
):
    """
    Find, read and concatenate CSV files, all with nice progress indication.

    Args:
        patterns (str): glob patterns (or just paths) to files to read. Will be
            flattened recursively, so can also be lists of patterns.
        progress (rich.progress.Progress, optional): the progress instance to
            use. Use ``False`` to hide the progress bar.
        console (rich.console.Console, optional): the console instance to use.
        concat_in_chunks (bool, optional): whether to do the concatenation in a
            tree of chunks. This enables displaying a progress but might also
            be slower. Defaults to True.
        reader (callable, optional): function taking a file handle as first
            argument and optional keyword arguments (``reader_kwargs``) and
            returning a dataframe. Note that you can access the file name via
            the file-handle's ``name`` attribute. Defaults to :any:`read_csv`.
        reader_pass_path_as (str, optional): pass an argument named like this
            to the reader containing the current path.
        merger (callable, optional): function taking an iterable of
            dataframes and returning a merged dataframe. Defaults to
            :any:`pandas.concat`
        to_numeric (bool or sequence of str, optional): whether to convert all
            (``True``), none (``False``), columns that don't produce too many
            NaNs when converted to numeric (``None``, the default) or a set of
            columns (sequence of glob patterns) with :any:`pandas.to_numeric`.
            These columns won't be considered for conversion for conversion to
            categorical.
        fix_parse_dates (bool, optional): convert ``parse_dates`` columns to
            datetime if it didn't work.
        categorical (bool, optional): whether to convert columns to the
            categorical type, options same as ``to_numeric``. The default
            (``None``) is to convert if it saves memory.
        units (dict or callable, optional): units mapping. Only applied to
            :any:`numpy.float64` columns. Mapping of (sequences of) regular
            expression patterns to :mod:`pint` units (from :mod:`parmesan`)
            or just ``True`` (or anything...) if a captured groups in the
            pattern yields the unit. Defaults to
            :any:`READ_CSV_FILES_UNIT_EXTRACTORS`.


            .. code-block:: python

                {
                    # simple patterns to unit mappings
                    "temperature": "°C",
                    "humidity.*sensor": "percent",
                    "humidity.*sensor": "percent",
                    # regular expression with captured groups yielding unit
                    re.compile(r"quantity \[([^\]])\]"): True,
                    ...
                }

            Or a function taking the (whole) column as argument and returning
            the unit.
        unit_sanitizer (callable, optional): function taking a unit string and
            returning is to :any:`pint` can understand it. Defaults to
            :any:`sanitize_units`.
        sort_index (bool, optional): whether to sort the index at the end.
            Defaults to True.
        **reader_kwargs : further arguments passed to the ``reader``.


    Returns:
        pandas.DataFrame : the merged dataframes
    """
    pbarcolumns = (
        MofNCompleteColumnGrowing(),
        BarColumn(),
        TaskProgressColumn(),
        TimeRemainingColumn(),
        TextColumn("[progress.description]{task.description}"),
    )
    counter = collections.Counter()
    if reader is pd.read_csv:
        reader_kwargs = {
            **dict(
                on_bad_lines="warn",
                encoding_errors="ignore",
            ),
            **reader_kwargs,
        }
    with progress or Progress(
        *pbarcolumns,
        console=console,
        expand=True,
        disable=True if progress is False else False,
    ) as progress:
        task_find = progress.add_task("🔎 Finding files...", total=None)
        patterns = sorted(set(utils.flatten(patterns)))
        if not patterns:
            patterns = set(("*.csv*", "*mosquitto*.log*", "*mqtt*.log*"))
            logger.warning(
                f"🤷 You didn't give any patterns/files. Assuming {', '.join(map(repr,patterns))}"
            )
        matches = set()
        matches_resolved = set()
        for n, path in enumerate(
            map(
                pathlib.Path,
                utils.flatten(glob.iglob(p, recursive=True) for p in patterns),
            ),
            start=1,
        ):
            if not path.exists():
                counter["missing"] += 1
            elif path.is_dir():
                counter["is-dir"] += 1
            elif path.resolve() in matches_resolved:
                counter["duplicate"] += 1
            else:
                matches.add(path)
                matches_resolved.add(path.resolve())
            progress.update(
                task_find,
                completed=len(matches),
                description=" ".join(
                    [f"🎯 Found {len(matches)} files"]
                    + ([f"#️⃣ {n} total"] if len(matches) != n else [])
                    + (
                        [f"♊ {counter['duplicate']} duplicates"]
                        if counter["duplicate"]
                        else []
                    )
                    + (
                        [f"📁 {counter['is-dir']} dirs"]
                        if counter["is-dir"]
                        else []
                    )
                    + (
                        [f"❓ {counter['missing']} missing"]
                        if counter["missing"]
                        else []
                    )
                ),
            )
        progress.update(task_find, completed=len(matches), total=len(matches))
        task_read_total = progress.add_task("📤 Read Files", total=len(matches))
        if not matches:
            logger.warning(f"🤷 No files to read. Returning empty dataframe.")
            progress.refresh()
            return pd.DataFrame()
        dataframes = []
        for n, path in enumerate(matches, start=1):
            task_read = progress.add_task(
                f"📤 Read {pathlib.Path(path).name!r}"
            )
            try:
                if mt := mimetype(path):
                    pass
                    # the below makes the progress bar janky
                    # progress.update(
                    #     task_read,
                    #     description=progress.tasks[
                    #         progress.task_ids.index(task_read)
                    #     ].description
                    #     + f" ({mt})",
                    # )
                op = opener(path, mimetype=mt)
                # open the raw file with rich for the tracking
                with progress.open(path, mode="rb", task_id=task_read) as fh_:
                    # wrap raw file with correct opener if necessary
                    fh = fh_ if op is open else op(fh_)
                    kw = reader_kwargs.copy()
                    if isinstance(reader_pass_path_as, str):
                        kw.update({reader_pass_path_as: path})
                    df = reader(fh, **kw)
                    if len(df.index) > 0:
                        dataframes.append(df)
                    else:
                        logger.warning(f"Skipping empty data from {path}")
            except Exception as e:
                logger.error(f"{type(e).__name__} reading {path}: {e}")
                counter["read-error"] += 1
            progress.update(
                task_read_total,
                description=" ".join(
                    [f"📤 Read Files"]
                    + (
                        [f"💥 {counter['read-error']} errors"]
                        if counter["read-error"]
                        else []
                    )
                ),
            )
            progress.update(task_read_total, advance=1)
            progress.remove_task(task_read)
        if not dataframes:
            logger.warning(
                f"🤷 Nothing to concatenate. Returning empty dataframe."
            )
            progress.refresh()
            return pd.DataFrame()
        task_concat = progress.add_task(
            "📚 Merging {len(dataframes)} dataframes", total=None
        )
        if concat_in_chunks:
            concat_per_step = max(math.ceil(len(dataframes) / 10), 3)
            progress.update(
                task_concat,
                total=utils.tree_joints(len(dataframes), concat_per_step),
            )
            while True:
                concat_steps = list(utils.take(dataframes, concat_per_step))
                progress.update(
                    task_concat,
                    description=f"📚 Merging {len(dataframes)} dataframes"
                    + (
                        f" in pairs of {concat_per_step}"
                        if len(dataframes) > concat_per_step
                        else ""
                    ),
                )
                dataframes_ = []
                for dfs in concat_steps:
                    dataframes_.append(merger(dfs))
                    del dfs[:]  # should free up memory
                    progress.update(task_concat, advance=1)
                dataframes = dataframes_
                if len(concat_steps) <= 1:
                    break
            progress.update(
                task_concat, description=f"📚 Merged all dataframes"
            )
            dataframe = dataframes[0]
        else:
            progress.update(task_concat)
            dataframe = merger(progress.track(dataframes, task_id=task_concat))
            progress.update(
                task_concat, completed=len(dataframes), total=len(dataframes)
            )
        to_numeric_columns = []
        if to_numeric is not False:
            if utils.is_iterable(to_numeric):
                columns = [
                    col
                    for col in dataframe
                    if any(fnmatch(col, p) for p in to_numeric)
                ]
            elif to_numeric in {None, True}:
                columns = tuple(dataframe)
            else:
                raise ValueError(
                    f"Invalid value {to_numeric = !r}. "
                    "Either True, False, None or sequence of globs."
                )
            task_num = progress.add_task("🔢 Converting to numeric")
            for col in progress.track(columns, task_id=task_num):
                progress.update(
                    task_num,
                    description=f"🔢 Converting {col!r} to numeric"
                    + ("?" if to_numeric is None else ""),
                )
                converted = pd.to_numeric(dataframe[col], errors="coerce")
                if to_numeric is None:
                    values_before = (~dataframe[col].isna()).sum()
                    converted = pd.to_numeric(dataframe[col], errors="coerce")
                    if (~converted.isna()).sum() / values_before < 0.75:
                        logger.debug(
                            f"{col!r} produces {converted.isna().sum()} "
                            "NaNs when converted to numerical, not converting it"
                        )
                        continue
                dataframe[col] = converted
                to_numeric_columns.append(col)
            progress.update(
                task_num,
                description=f"🔢 Converted {len(to_numeric_columns)} columns to numeric",
            )
        to_categorical_columns = []
        if categorical is not False:
            memory_usage_before = dataframe.memory_usage(deep=True).sum()
            if utils.is_iterable(categorical):
                columns = [
                    col
                    for col in dataframe
                    if any(fnmatch(col, p) for p in to_numeric)
                ]
            elif categorical in {None, True}:
                columns = tuple(dataframe)
            else:
                raise ValueError(
                    f"Invalid value {categorical = !r}. "
                    "Either True, False, None or sequence of globs."
                )
            columns = [col for col in columns if col not in to_numeric_columns]
            task_cat = progress.add_task(
                "📦 Converting to categorical",
                total=len(columns),
            )
            for col in columns:
                cat = None
                convert = categorical
                if categorical is None:
                    progress.update(
                        task_cat,
                        description=f"📦 Checking if {col!r} is better categorical",
                    )
                    if (cat := dataframe[col].astype("category")).memory_usage(
                        deep=True
                    ) < dataframe[col].memory_usage(deep=True):
                        convert = True
                if convert:
                    progress.update(
                        task_cat,
                        description=f"📦 Converting {col!r} to categorical",
                    )
                    dataframe[col] = (
                        dataframe[col].astype("category")
                        if cat is None
                        else cat
                    )
                    to_categorical_columns.append(col)
                progress.update(task_cat, advance=1)
            memory_usage_after = dataframe.memory_usage(deep=True).sum()
            if (saved_bytes := memory_usage_before - memory_usage_after) > 0:
                saved = "{value:.1f} {unit}".format(
                    value=saved_bytes
                    / 1000 ** (n := (int(np.log10(saved_bytes)) // 3)),
                    unit=["bytes", "kB", "MB", "GB", "TB"][n],
                )
                progress.update(
                    task_cat,
                    description=f"📦 Saved {(1 - memory_usage_after/memory_usage_before)*100:.1f}% "
                    f"({saved}) memory with categorical",
                )
            else:
                progress.update(
                    task_cat,
                    description=f"📦 Converting to categorical isn't worth it"
                    if to_categorical_columns
                    else f"📦 No columns converted to categorical",
                )
        unit_added_columns = []
        numerical_columns = []
        for col in dataframe:
            if isinstance(dataframe[col].dtype, pd.CategoricalDtype):
                continue  # np.issubdtype doesn't understand this one
            try:
                if np.issubdtype(dataframe[col].dtype, float) or np.issubdtype(
                    dataframe[col].dtype, int
                ):
                    numerical_columns.append(col)
            except Exception as e:
                logger.error(
                    f"Couldn't determine if {col!r} is a numeric type: {e!r}"
                )
        if (
            units
            and numerical_columns
            and isinstance(dataframe.columns, pd.MultiIndex)
            and not hasattr(units, "__call__")
        ):
            logger.warning(
                f"Reading units from a MultiIndex is currently not implemented. "
                f"But you can specify a callable as units= that takes the whole column and returns the unit. "
                f"Setting units=False."
            )
            units = False
        if units and numerical_columns:
            task_units = progress.add_task("🌡️ Adding units")
            if hasattr(units, "__call__"):
                for col in progress.track(
                    numerical_columns, task_id=task_units
                ):
                    unit = units(dataframe[col])
                    if unit_sanitizer:
                        unit = unit_sanitizer(unit)
                    try:
                        dataframe[col] = dataframe[col].astype(f"pint[{unit}]")
                    except Exception as e:
                        logger.error(
                            f"⚠️  Can't convert {col!r} to unit {unit!r}: {e!r}"
                        )
                        continue
                    progress.update(
                        task_units,
                        description=f"🌡️ {col!r} has unit {unit!r}",
                    )
                    unit_added_columns.append(col)
            else:
                units = {
                    (re.compile(p) if isinstance(p, str) else p): u
                    for p, u in units.items()
                }
                for col in progress.track(
                    numerical_columns, task_id=task_units
                ):
                    for pattern, unit in units.items():
                        if m := pattern.search(col):
                            unit = next(iter(m.groups()), unit)
                            if unit_sanitizer:
                                unit = unit_sanitizer(unit)
                            try:
                                dataframe[col] = dataframe[col].astype(
                                    f"pint[{unit}]"
                                )
                                progress.update(
                                    task_units,
                                    description=f"🌡️ {col!r} has unit {unit!r}",
                                )
                                unit_added_columns.append(col)
                                break  # go on with next column
                            except Exception as e:
                                logger.error(
                                    f"⚠️  Can't convert {col!r} to unit {unit!r}: {e!r}"
                                )
                                continue  # with next pattern
            progress.update(
                task_units,
                description=f"🌡️ Added units to {len(unit_added_columns)} columns",
            )
        if (
            parse_dates := reader_kwargs.get("parse_dates")
        ) and fix_parse_dates:
            task_to_datetime = progress.add_task(
                "📅 Checking {len(parse_dates)} parse_dates columns"
            )
            fixed = 0
            for col in progress.track(
                parse_dates,
                task_id=task_to_datetime,
            ):
                if not isinstance(col, str):
                    logger.warning(
                        f"Can't handle parse_dates value {col!r} (only strings for now...)"
                    )
                    continue
                if col in dataframe.columns and not hasattr(
                    dataframe[col], "strftime"
                ):
                    progress.update(
                        task_to_datetime,
                        description=f"📅 Converting {col!r} ({dataframe[col].dtype}) to datetime",
                    )
                    dataframe[col] = pd.to_datetime(
                        dataframe[col], errors="coerce"
                    )
                    fixed += 1
                    continue
                if col == dataframe.index.name and not hasattr(
                    dataframe.index, "strftime"
                ):
                    progress.update(
                        task_to_datetime,
                        description=f"📅 Converting index {col!r} ({dataframe.index.dtype}) to datetime",
                    )
                    dataframe.index = pd.to_datetime(
                        dataframe.index, errors="coerce"
                    )
                    fixed += 1
                    continue
            if fixed:
                progress.update(
                    task_to_datetime,
                    description=(
                        f"📅 Fixed {fixed} broken parse_dates columns"
                        if fixed
                        else f"📅 No parse_dates columns were broken"
                    ),
                )
            else:
                progress.remove_task(task_to_datetime)
        if sort_index:
            task_sort = progress.add_task("↕️ Sorting by index", total=None)
            dataframe.sort_index(inplace=True)
            progress.update(
                task_sort, description="↕️ Sorted index", completed=1, total=1
            )
        progress.refresh()
        return dataframe


def read_mosquitto_logs(files, **read_csv_kwargs):
    """
    Read a list of mosquitto logfiles in this format:

    .. code-block::csv

        2022-02-07T06:47:02.590704+0000 station/station01/Sd/status Ok
        2022-02-07T06:47:02.591040+0000 station/station01/RTC/status notFound
        2022-02-07T06:47:02.591091+0000 station/station01/70/bme280/works False
        2022-02-07T06:47:02.591141+0000 station/station01/70/scd30/works False
        2022-02-07T06:47:02.591190+0000 station/?/70/bme280/works True
        2022-02-07T06:47:02.591238+0000 station/?/70/scd30/works True
        2022-02-07T06:47:02.591286+0000 station/?/50/scd30/works True
        2022-02-07T06:47:02.591333+0000 station/2/50/bme280/works False
        2022-02-07T06:47:02.591381+0000 station/2/50/scd30/works False
        ...

    Args:

        files (str or list of str): paths to log files so that
            :any:`pandas.read_csv` can understand it. This includes appropriate
            compressed files. If ``files`` is a :any:`str`, it is first fed
            into :any:`glob.glob`.

    Example
    =======

    .. code-block:: python

        import glob
        from co2project.input import read_mosquitto_logs
        read_mosquitto_logs("data/*mosquitto*.log*")

        #                                              topic  \\
        # time
        # 2022-02-02 11:53:09.691555+00:00  al/rpi/usv/power
        # 2022-02-02 11:53:09.691757+00:00  network/devices
        # 2022-02-02 11:53:09.691888+00:00  a/port/1/bme/...
        # 2022-02-02 11:53:09.691956+00:00  r/port/1/status
        # 2022-02-02 11:53:09.692021+00:00  /port/1/scd/...

        #                                                   message
        # time
        # 2022-02-02 11:53:09.691555+00:00                       ok
        # 2022-02-02 11:53:09.691757+00:00                        1
        # 2022-02-02 11:53:09.691888+00:00                not-found
        # 2022-02-02 11:53:09.691956+00:00                        ?
        # 2022-02-02 11:53:09.692021+00:00  parent-multiplexer-fail


    .. hint::

        Try passing ``skiprows=10`` (or more) to skip some rows if you get
        errors like ``NotImplementedError: file structure not yet supported``.

    .. hint::

        You might want to convert the ``topic`` into columns with
        :any:`breakout_column`:

        .. code-block:: python

            df = read_mosquitto_logs("data/*.log*")
            # only keep relevant topics
            df = df[df.topic.isin(["topic1","topic2","..."])]
            # might want to drop the unit colum
            df = df.drop(columns=["unit"])
            # turn topics into columns
            df = df.co2project.breakout_column("topic")
            # now you can access the topics as columns
            df["topic1"]
            # might want to resample to get the index nice and sorted
            df = df.resample("1min").mean()

    """
    warnings.warn(
        f"read_mosquitto_logs() is deprecated, use read_csv_files() instead. "
        f"Using read_csv_files() is used under the hood now.",
        category=DeprecationWarning,
    )
    return read_csv_files(
        files,
        **{
            **dict(
                delimiter=r"\s+",
                on_bad_lines="skip",
                parse_dates=["time"],
                index_col="time",
                names=(
                    "time",
                    "topic",
                    "message",
                    "unit",
                    "wtf",
                    "wtf2",
                    "wtf3",
                ),
                dtype="category",
            ),
            **read_csv_kwargs,
        },
    )


COLUMN_UNITS = {
    "Gill Maximet GMX541": {
        "AH": "g/m³",
        "AIRDENS": "kg/m³",
        "AVGCDIR": "°",
        "AVGCSPEED": "m/s",
        "AVGDIR": "°",
        "AVGSPEED": "m/s",
        "CDIR": "°",
        "CGDIR": "°",
        "CGSPEED": "m/s",
        "COMPASSH": "°C",
        "CSPEED": "m/s",
        "DEWPOINT": "°C",
        "DIR": "°",
        "GDIR": "°",
        "GPSHEADING": "°C",
        "GPSSPEED": "m/s",
        "GSPEED": "m/s",
        "HEATIDX": "°C",
        "PASL": "hPa",
        "PRECIPI": "mm",
        "PRECIPT": "mm",
        "PRESS": "hPa",
        "PSTN": "hPa",
        "RH": "percent",
        "SOLARHOURS": "hour",
        "SOLARRAD": "W/m²",
        "SPEED": "m/s",
        "TEMP": "°C",
        "VOLT": "V",
        "WBTEMP": "°C",
        "WCHILL": "°C",
        "XTILT": "°",
        "YTILT": "°",
    },
    # Incomplete!
    # See https://s.campbellsci.com/documents/us/manuals/easyflux-dl-cr6op.pdf
    "Flux_AmeriFluxFormat": {
        "CO2": "umol/mol",
        "C2O_SIGMA": "umol/mol",
        "H2O": "mmol/mol",
        "H2O_SIGMA": "mmol/mol",
        "FC": "umol/m²/s",
        "LE": "W/m²",
        "ET": "mm/hour",
        "H": "W/m²",
        "G": "W/m²",
        "SG": "W/m²",
        "FETCH_MAX": "m",
        "FETCH_90": "m",
        "FETCH_55": "m",
        "FETCH_40": "m",
        "WD": "°",
        "WS": "m/s",
        "WS_MAX": "m/s",
        "USTAR": "m/s",
        "TAU": "kg/m/s²",
        "MO_LENGTH": "m",
        "U": "m/s",
        "V": "m/s",
        "W": "m/s",
        "U_SIGMA": "m/s",
        "V_SIGMA": "m/s",
        "W_SIGMA": "m/s",
        "PA": "kPa",
        "TA_1_1_1": "°C",
        "RH_1_1_1": "percent",
        "T_DP_1_1_1": "°C",
        "TA_1_1_2": "°C",
        "RH_1_1_2": "percent",
    },
}
