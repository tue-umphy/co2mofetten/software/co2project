# system modules
import functools
import math
import io

# ineternal modules

# external modules


class CallbackBufferedReader(io.BufferedReader):
    def __init__(self, *args, on_read=None, **kwargs):
        io.BufferedReader.__init__(self, *args, **kwargs)
        self.on_read = on_read

    @staticmethod
    def call_on_read_afterwarts(decorated_function):
        @functools.wraps(decorated_function)
        def wrapper(self, *args, **kwargs):
            ret = decorated_function(self, *args, **kwargs)
            if self.on_read:
                self.on_read(self, ret, *args, **kwargs)
            return ret

        return wrapper

    @call_on_read_afterwarts
    def read1(self, size=-1):
        return io.BufferedReader.read1(self, size)

    @call_on_read_afterwarts
    def read(self, size=-1):
        return io.BufferedReader.read(self, size)


def is_iterable(x):
    """
    Check if a given object is iterable but not a string.
    """
    if isinstance(x, str):
        return False
    try:
        iter(x)
    except TypeError:
        return False
    return True


def iter_(x):
    """
    Like :any:`iter` but yield uniterable objects instead of raising a
    :any:`TypeError`.
    """
    if isinstance(x, str):
        yield x
    try:
        yield from x
    except TypeError:
        yield x


def flatten(*sequences):
    """
    Flatten given sequence(s)
    """
    if len(sequences) > 1:
        for sequence in sequences:
            yield from flatten(sequence)
    else:
        sequence = next(iter_(sequences), tuple())
        if isinstance(sequence, str):
            yield sequence
        try:
            for element in sequence:
                if is_iterable(element):
                    yield from flatten(element)
                else:
                    yield element
        except TypeError:
            yield sequence


def take(iterable, n):
    """
    Yield n items of an iterable at a time
    """
    iterable = iter(iterable)
    while True:
        parts = []
        for i in range(n):
            try:
                parts.append(next(iterable))
            except StopIteration:
                if parts:
                    yield parts
                return
        yield parts


def tree_joints(n, splits=2):
    joints = 0
    while True:
        x = int(math.ceil(n / splits))
        joints += x
        if (n := x) <= 1:
            break
    return joints
